<?php

namespace Sootlib\Util;

use Sootlib\Util\Exceptions\KeyNotInArrayException;

/**
 * ConfigUtil is a utlity class to interact with config files
 * 
 * It's methods are static so don't bother instatiating it.
 * 
 * @package Sootlib
 * @subpackage Util
 */
abstract class Config {
    public abstract static function get_config_value($keyname ,$config_filepath);
}
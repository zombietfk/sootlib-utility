<?php

namespace Sootlib\Util;

class Formatter {
    private static function R_ArraToXML($array, $node_name) {
        $xml = '';
        if (is_array($array) || is_object($array)) {
            foreach ($array as $key=>$value) {
                if (is_numeric($key)) {
                    $key = $node_name;
                }

                $xml .= '<' . $key . '>' . self::R_ArraToXML($value, $node_name) . '</' . $key . '>';
            }
        } else {
            $xml = htmlspecialchars($array, ENT_QUOTES);
        }
        return $xml;
    }

    public static function AsXMLFormat($object, $node_block='nodes', $node_name='node') {
        $array = NULL;
        if(is_object($object)){
            $array = get_object_vars($object);
        } else {
            $array = $object;
        }
        header("Content-Type: application/xml");
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<' . $node_block . '>';
        $xml .= self::R_ArraToXML($array, $node_name);
        $xml .= '</' . $node_block . '>';
        return $xml;
    }

    public static function AsJSONFormat($object, $utfencode = false){
        if($utfencode) $object = Formatter::Utf8EncodeRecursive($object);
        return json_encode($object);
    }

    private static function Utf8EncodeRecursive($input) {
        if (is_string($input)) {
                $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as $key=>$value) {
                $input[$key] = Formatter::Utf8EncodeRecursive($value);
            }
            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            foreach ($vars as $key=>$var) {
                $input[$key] = Formatter::Utf8EncodeRecursive($input->$var);
            }
        }
        return $input;
    }
}

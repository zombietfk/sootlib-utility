<?php

namespace Sootlib\Util;

use Exception;
use Throwable;

class Fastpost {

    public static function Get($url, $args = array(), $headers = array()){
        $postbody = "";
        $first_pass = true;
        foreach($args as $key=>$arg){
            if(!$first_pass){
                $postbody .= "&";
            }
            $first_pass = false;
            $postbody .= ($key."=".$arg);
        }
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL => $url,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_FOLLOWLOCATION => TRUE
            )
        );
        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($status_code != 200 && $status_code != 204 && $status_code != 202) {
            throw new Exception("Error : Returned Status Code " . $status_code . ", unable to communicate with server");
        }
        return $result;
    }

    public static function Post($url, $args = array(), $headers = array()){
        $postbody = "";
        $first_pass = true;
        foreach($args as $key=>$arg){
            if(!$first_pass){
                $postbody .= "&";
            }
            $first_pass = false;
            $postbody .= ($key."=".$arg);
        }
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL => $url,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $postbody,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_FOLLOWLOCATION => TRUE
            )
        );
        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($status_code != 200 && $status_code != 204 && $status_code != 202) {
            throw new Exception("Error : Returned Status Code " . $status_code . ", unable to communicate with server");
        }
        return $result;
    }

    public static function Delete($url, $args = array(), $headers = array()) {
        $postbody = "";
        $first_pass = true;
        foreach($args as $key=>$arg){
            if(!$first_pass){
                $postbody .= "&";
            }
            $first_pass = false;
            $postbody .= ($key."=".$arg);
        }
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL => $url,
                CURLOPT_CUSTOMREQUEST => "DELETE",
                CURLOPT_POSTFIELDS => $postbody,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_FOLLOWLOCATION => TRUE
            )
        );
        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($status_code != 200 && $status_code != 204 && $status_code != 202){
            throw new Exception("Error : Returned Status Code " . $status_code . ", unable to communicate with server");
        }
        return $result;
    }

    public static function Put($url, $args = array(), $headers = array()){
        $postbody = "";
        $first_pass = true;
        foreach($args as $key=>$arg){
            if(!$first_pass){
                $postbody .= "&";
            }
            $first_pass = false;
            $postbody .= ($key."=".$arg);
        }
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL => $url,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded"
                ),
                CURLOPT_POSTFIELDS => $postbody,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_FOLLOWLOCATION => TRUE
            )
        );
        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($status_code != 200) {
            throw new Exception("Error : Returned Status Code " . $status_code . ", unable to communicate with server");
        }
        return $result;
    }
}

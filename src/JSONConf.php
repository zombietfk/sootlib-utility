<?php

namespace Sootlib\Util;

use Sootlib\Util\Exceptions\KeyNotInArrayException;

class JSONConf extends Config{

    public static function get_config_value($keyname ,$config_filepath) {
        $config = file_get_contents($config_filepath);
        $config_array = json_decode($config, true);
        $value = JSONConf::r_array_key_search($config_array, $keyname);
        return $value;
    }

    private static function r_array_key_search($map, $forkey, $n = 0){
        foreach($map as $k=>$v){
            if($k == $forkey){
                return $v;
            } else if(is_array($v)){
                JSONConf::r_array_key_search($v, $forkey, $n + 1);
            }
        }
        if($n == 0) {
            throw new KeyNotInArrayException("given key does not exist in the array provided");
        }
    }
}
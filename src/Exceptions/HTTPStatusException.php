<?php

namespace Sootlib\Util\Exceptions;

use Exception;

class HTTPStatusException extends Exception {
	public function __construct($message = "", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
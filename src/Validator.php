<?php
/**
 * Created by PhpStorm.
 * User: sam.judge
 * Date: 21/11/2017
 * Time: 10:54
 */

namespace Sootlib\Util;

class Validator {
	public static function GetFromOrNull($requestbody, $token){
		if(isset($requestbody[$token])){
			return $requestbody[$token];
		} else {
			return null;
		}
	}
}
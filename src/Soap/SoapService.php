<?php
//use this to use any soap service that uses WSDL

namespace Sootlib\Util\Soap;

class SoapService {

    private $soap_server;

    public $wsdl;
    /**
     * quinyx constructor
     * @param $wdsl_uri string - the address of the quinyx server
     */
    public function __construct($wsdl) {
        $this->wsdl = $wsdl;
        $this->soap_server = new SoapServer($wsdl);
    }

    public function __call($name, $args){
		try {
			$this->soap_server->addFunction($name);
			$this->soap_server->handle();
			$soap_client = new SoapClient($this->wsdl);
			$result = call_user_func_array(array($soap_client, $name), $args);
			return $result;
		} catch(SoapFault $e){
			echo json_encode(array(
				"error"=>$e->getMessage()
			));
			die;
			
		}
    }

}